//
//  GiphyApi.swift
//  Copyright © 2020 Orken Serik. All rights reserved.
//

import Foundation
import Moya

enum GiphyApi {
    case register(params: [String: Any])
    case search(name: String)
}

extension GiphyApi: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://petstore.swagger.io/v2/") else {
            fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .register:
            return "user"
        case .search(let name):
            return "user/\(name)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .register:
            return .post
        default:
            return .get
        }
    }
    
    var sampleData: Data {
//        return Data()
        return "".data(using: String.Encoding.utf8)!
    }
    
    var parameters: [String : Any] {
        switch self {
            
        default:
            return [:]
        }
    }
    
    var task: Task {
        
        switch self {
        case .register(let params):
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
            
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var header: [String : String]  = [
            "Content-type": "application/json",
            "Accept":"application/json"
            ]
        return header
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}



