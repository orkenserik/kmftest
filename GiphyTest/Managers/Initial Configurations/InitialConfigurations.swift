//
//  Initial Configurations.swift
//  GiphyTest
//
//  Created by Orken Serik on 03.08.2021.
//

import Foundation
import IQKeyboardManager
import UIKit

protocol InitialConfigurationsProtocol: class {
    
    func load(application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?, window: UIWindow?)
    
}

class InitialConfigurations: InitialConfigurationsProtocol {
    func load(application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey : Any]?, window: UIWindow?) {
        configureKeyboardManager()
        initiateApp(window: window)
        
    }
    
}

//MARK: - Methods
extension InitialConfigurations {
    
    private func configureKeyboardManager() {
        let keyboardManager = IQKeyboardManager.shared()
        keyboardManager.shouldShowToolbarPlaceholder = false
        keyboardManager.shouldResignOnTouchOutside = true
    }
    
    private func initiateApp(window: UIWindow?) {
        let vc = MainPageController(viewModel: MainPageViewModel())
        let navController = UINavigationController(rootViewController: vc)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}
