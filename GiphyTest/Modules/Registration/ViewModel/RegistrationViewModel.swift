//
//  RegistrationViewModel.swift
//  GiphyTest
//
//  Created by Orken Serik on 01.09.2021.
//

import Foundation

final class RegistrationViewModel {
    
}

//MARK: - Methods
extension RegistrationViewModel {
    func registerUser(params: [String: Any], _ success: @escaping () -> Void, _ failure: @escaping failure) {
        ApiClient.shared.makeRequest(GiphyApi.register(params: params)) { (response) in
            success()
        } _: { (error, errorField) -> ()? in
            failure(error)
        }

    }
}
