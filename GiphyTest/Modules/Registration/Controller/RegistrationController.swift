//
//  RegistrationController.swift
//  GiphyTest
//
//  Created by Orken Serik on 01.09.2021.
//

import Foundation
import UIKit
import SnapKit

class RegistrationController: UIViewController {
    
    private lazy var usernameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Юзернейм"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Имя"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var lastnameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Фамилия"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Почта"
        textField.keyboardType = .emailAddress
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Пароль"
        textField.isSecureTextEntry = true
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var phoneNumberTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Номер телефона(без 8)"
        textField.keyboardType = .decimalPad
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    private lazy var statusTextField: UITextField = {
        let textField = UITextField()
        textField.keyboardType = .decimalPad
        textField.placeholder = "Статус"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        return textField
    }()
    
    
    private lazy var regisButton: UIButton = {
        let button = UIButton()
        button.setTitle("Регистрация", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    
    private let viewModel: RegistrationViewModel
    
    // MARK: - Init
    init(viewModel: RegistrationViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
}

//MARK: - Methods
extension RegistrationController {
    
    @objc private func register() {
        
        if !(emailTextField.text?.isValidEmail())! {
            showError("Wrong Email")
            return
        }

        if !(phoneNumberTextField.text?.isValidPhoneNumber())! {
            showError("Wrong Phone Number")
            return
        }
        
        let status = Int(statusTextField.text ?? "") ?? 1
        if status > 5 || status < 1 {
            showError("Статус может принимать значения от 1 до 5")
            return
        }
        
        
        var params : [String: Any] = [:]
        params["id"] = 0
        params["username"] = usernameTextField.text
        params["firstName"] = nameTextField.text
        params["lastName"] = lastnameTextField.text
        params["email"] = emailTextField.text
        params["password"] = passwordTextField.text
        params["phone"] = phoneNumberTextField.text
        params["userStatus"] = statusTextField.text
        viewModel.registerUser(params: params) { [weak self] in
            guard let `self` = self else {return}
            self.showAlert("Successfully Registered", "") {
                self.navigationController?.popViewController(animated: true)
            }
        } _: { (error) in
            self.showError(error)
        }

    }
    
}

//MARK: - UI
extension RegistrationController {
    
    private func configUI() {
        view.backgroundColor = .white
        
        [usernameTextField, nameTextField, lastnameTextField, emailTextField, passwordTextField, phoneNumberTextField, statusTextField, regisButton].forEach {
            view.addSubview($0)
        }
        
        usernameTextField.snp.makeConstraints { (make) in
            make.topMargin.equalTo(30)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        nameTextField.snp.makeConstraints { (make) in
            make.top.equalTo(usernameTextField.snp.bottom).offset(16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        lastnameTextField.snp.makeConstraints { (make) in
            make.top.equalTo(nameTextField.snp.bottom).offset(16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        emailTextField.snp.makeConstraints { (make) in
            make.top.equalTo(lastnameTextField.snp.bottom).offset(16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(emailTextField.snp.bottom).offset(16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        phoneNumberTextField.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom).offset(16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        statusTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneNumberTextField.snp.bottom).offset(16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(44)
        }
        
        regisButton.snp.makeConstraints { (make) in
            make.top.equalTo(statusTextField.snp.bottom).offset(25)
            make.height.equalTo(50)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
    }
}
