//
//  UserListViewModel.swift
//  GiphyTest
//
//  Created by Orken Serik on 03.08.2021.
//

import Foundation

final class UserListViewModel {
    var userList: [User] = []
}

//MARK: - Request
extension UserListViewModel {
    func searchUser(name: String, success: @escaping () -> Void, _ failure: @escaping failure) {
        ApiClient.shared.makeRequest(GiphyApi.search(name: name), model: User.self, requestType: .model) { (model, array) in
            if let user = model {
                self.userList.append(user)
            }
            success()
        } _: { (error, errorMessage) -> ()? in
            failure(error)
        }


    }
}
