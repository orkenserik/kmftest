//
//  GifCell.swift
//  GiphyTest
//
//  Created by Orken Serik on 03.08.2021.
//

import Foundation
import UIKit

import UIKit

class UserCell: UITableViewCell {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var usernameLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .darkGray
        label.textAlignment = .left
        label.numberOfLines = 2
        return label
    }()
    
    lazy var fullnameLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .darkGray
        label.textAlignment = .left
        return label
    }()
    
    lazy var emailLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .darkGray
        label.textAlignment = .left
        return label
    }()
    
    lazy var phoneLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .darkGray
        label.textAlignment = .left
        label.numberOfLines = 2
        return label
    }()
    
    lazy var statusLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .darkGray
        label.textAlignment = .left
        label.numberOfLines = 2
        return label
    }()
    
    //MARK: - INIT
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        configUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Methods
extension UserCell {
    
    func setCell(_ item: User) {
        usernameLabel.text = "Username: \(item.username)"
        fullnameLabel.text = "\(item.firstName)" + " " + "\(item.lastName)"
        emailLabel.text = "eMail: \(item.email)"
        phoneLabel.text = "Phone Number: \(item.phone)"
        statusLabel.text = "Status: \(item.userStatus)"
    }
}

//MARK: - UI
extension UserCell {
    
    func configUI() {
        self.backgroundColor = .lightGray
        
        self.addSubview(containerView)
        [usernameLabel, fullnameLabel, emailLabel, phoneLabel, statusLabel].forEach({
            containerView.addSubview($0)
        })
        
        setConstraints()
    }
    
    private func setConstraints() {
        containerView.snp.makeConstraints { (make) in
            make.top.left.equalTo(16)
            make.right.equalTo(-16)
            make.bottom.equalToSuperview()
        }
        
        usernameLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(16)
            make.right.equalTo(-16)
        }
        
        fullnameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(usernameLabel.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
        
        emailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(fullnameLabel.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
        
        phoneLabel.snp.makeConstraints { (make) in
            make.top.equalTo(emailLabel.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
        
        statusLabel.snp.makeConstraints { (make) in
            make.top.equalTo(phoneLabel.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.bottom.equalTo(-16)
        }
        
    }
}




