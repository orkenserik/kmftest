//
//  UserListController.swift
//  GiphyTest
//
//  Created by Orken Serik on 03.08.2021.
//

import Foundation
import UIKit

class UserListController: UIViewController {
    
    private lazy var searchBar : UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.backgroundColor = .white
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = "Search Username"
        searchBar.searchTextPositionAdjustment = UIOffset(horizontal: 15, vertical: 0)
        
        searchBar.setImage(UIImage(named: "icn_back_gray"), for: .search, state: .normal)
        searchBar.delegate = self
        
        var textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .black
        textFieldInsideSearchBar?.backgroundColor = .white
        textFieldInsideSearchBar?.font = UIFont.systemFont(ofSize: 16)
        
        return searchBar
    }()
    
    fileprivate lazy var tableView : UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UserCell.self, forCellReuseIdentifier: App.CellIdentifier.userCellId)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .lightGray
        tableView.separatorStyle = .none
        let tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        tableFooterView.backgroundColor = .lightGray
        tableView.tableFooterView = tableFooterView
        return tableView
    }()
    
    private let viewModel: UserListViewModel
    
    // MARK: - Init
    init(viewModel: UserListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setData()
    }
}

//MARK: - Methods
extension UserListController {
    func searchUser(_ text: String) {
        viewModel.searchUser(name: text) { [weak self] in
            guard let `self` = self else {return}
            DispatchQueue.main.async {
                self.setData()
            }
            
        } _: { [weak self] (errorMessage) in
            guard let `self` = self else {return}
            self.showError(errorMessage)
        }
    }
    
    func setData() {
        self.tableView.reloadData()
        self.searchBar.text = ""
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension UserListController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: App.CellIdentifier.userCellId, for: indexPath) as! UserCell
        let user = viewModel.userList[indexPath.row]
        cell.setCell(user)
        return cell
    }
    
}

extension UserListController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        searchUser(text)
    }
}



//MARK: - UI
extension UserListController {
    
    private func configUI() {
        view.addSubview(searchBar)
        view.addSubview(tableView)
        setConstraints()
    }
    
    private func setConstraints() {
        searchBar.snp.makeConstraints { (make) in
            make.top.equalTo(65)
            make.left.right.equalToSuperview()
            make.height.equalTo(50)
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }
}

