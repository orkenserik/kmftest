//
//  MainPageController.swift
//  GiphyTest
//
//  Created by Orken Serik on 01.09.2021.
//

import Foundation
import UIKit
import SnapKit

class MainPageController: UIViewController {
    
    private lazy var regisButton: UIButton = {
        let button = UIButton()
        button.setTitle("Регистрация", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    
    private lazy var userInfoButton: UIButton = {
        let button = UIButton()
        button.setTitle("Информация о пользователе", for: .normal)
        button.backgroundColor = .darkGray
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(showUserInfo), for: .touchUpInside)
        return button
    }()
    
    private let viewModel: MainPageViewModel
    
    // MARK: - Init
    init(viewModel: MainPageViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
}

//MARK: - Methods
extension MainPageController {
    
    @objc private func register() {
        let vc = RegistrationController(viewModel: RegistrationViewModel())
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func showUserInfo() {
        let vc = UserListController(viewModel: UserListViewModel())
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//MARK: - UI
extension MainPageController {
    
    private func configUI() {
        view.backgroundColor = .white
        view.addSubview(regisButton)
        view.addSubview(userInfoButton)
        
        regisButton.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(50)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
        
        userInfoButton.snp.makeConstraints { (make) in
            make.top.equalTo(regisButton.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(50)
        }
    }
}
