//
//  User.swift
//  GiphyTest
//
//  Created by Orken Serik on 01.09.2021.
//

import Foundation

struct User: Codable {
    let id: Double
    let username, firstName, lastName, email: String
    let password, phone: String
    let userStatus: Int
}
